//
//  GameViewController.swift
//  LejlaBelma
//
//  Created by Ermin on 2016-05-20.
//  Copyright (c) 2016 Ermin Mahmutovic. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GameViewController: UIViewController {
    
 var player : AVAudioPlayer?   

    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = StartGameScene(size:CGSize(width: 1536,height: 2048))
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            skView.presentScene(scene)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        playBgSound()
        
    }
    // Playing soung fuc
    func playBgSound(){
        let url = NSBundle.mainBundle().URLForResource("space2", withExtension: "mp3")
        do{
            player = try AVAudioPlayer(contentsOfURL:url!)
            guard let player = player else{return}
            player.prepareToPlay()
            player.play()
            
        }catch let error as NSError{
            print(error.description)
        }
    }
    

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
