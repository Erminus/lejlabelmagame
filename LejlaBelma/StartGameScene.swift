//
//  StartGameScene.swift
//  LejlaBelma
//
//  Created by Ermin on 2016-05-22.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

import UIKit
import SpriteKit

class StartGameScene: SKScene {
        // start button label
    let startButton = PulsatingText(fontNamed:"ChalkDuster")
        // Load view
    override func didMoveToView(view: SKView) {
            // Adding startfield background
        let starField = SKEmitterNode(fileNamed:"StarField")
        starField?.position = CGPoint(x:self.size.width / 2, y:self.size.height / 2)
        starField?.zPosition = -100
        self.addChild(starField!)
            // Addding start button label
        startButton.makeTextPulsating("Start", theFontSize: 90)
        startButton.zPosition = 1
        startButton.position = CGPoint(x:self.size.width / 2, y: self.size.height * 0.4)
        self.addChild(startButton)
        
    }
    // Start button
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
            // Looping all touches and check cordinate of self
        for touch in touches{
            let touchPoint = touch.locationInNode(self)
                // Checking if touch cordinate ic start button
            if startButton.containsPoint(touchPoint){
                // Sending to gamescene
                let sceneToMoveTo = GameScene(size:self.size)
                sceneToMoveTo.scaleMode = scaleMode
                // Make trasition type to move
                let trasitionType  = SKTransition.doorsOpenVerticalWithDuration(0.5)
                // Presenting the view
                self.view!.presentScene(sceneToMoveTo, transition: trasitionType)
            }
        }
    }
}
