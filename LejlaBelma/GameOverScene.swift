//
//  GameOverScene.swift
//  LejlaBelma
//
//  Created by Ermin on 2016-05-21.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
    
    let restartLabel = PulsatingText(fontNamed:"ChalkDuster")
    let quiteLabel = SKLabelNode(fontNamed:"ChalkDuster")
    
    override func didMoveToView(view: SKView) {
        // Adding startfields
        let starField = SKEmitterNode(fileNamed:"StarField")
        starField?.position = CGPoint(x:self.size.width / 2, y:self.size.height / 2)
        starField?.zPosition = -100
        self.addChild(starField!)
            // Game over laber
        let gameOverLabel = SKLabelNode(fontNamed:"ChalkDuster")
            // Text to that label
        gameOverLabel.text = "Game Over"
        gameOverLabel.fontColor = SKColor.whiteColor()
        gameOverLabel.fontSize = 150
        gameOverLabel.position = CGPoint(x:self.size.width * 0.5,y:self.size.height * 0.7)
        gameOverLabel.zPosition = 1;
        self.addChild(gameOverLabel)
            // Setting up the score label
        let scoreLabel = SKLabelNode(fontNamed:"ChalkDuster")
        scoreLabel.text = "Score: \(gameScore)"
        scoreLabel.fontSize = 100
        scoreLabel.fontColor = SKColor.whiteColor()
        scoreLabel.position = CGPoint(x:self.size.width / 2, y: self.size.height * 0.55)
        scoreLabel.zPosition = 1
        self.addChild(scoreLabel)
        
            // using nsuserdefault to store hightscore
        let defaults = NSUserDefaults()
            // Adding a var to store all hightScore
        var hightScore = defaults.integerForKey("highScoreSaved")
        
            // Cheking if highScore is greater than gmane score
        if gameScore > hightScore {
            hightScore = gameScore
            defaults.setInteger(hightScore, forKey: "highScoreSaved")
        }
        
            // Setting up hightscore label
        let highScoreLabel = SKLabelNode(fontNamed:"ChalkDuster")
        highScoreLabel.text = "High Score: \(hightScore)"
        highScoreLabel.fontSize = 100
        highScoreLabel.fontColor = SKColor.whiteColor()
        highScoreLabel.position = CGPoint(x:self.size.width / 2, y: self.size.height * 0.45)
        highScoreLabel.zPosition = 1
        self.addChild(highScoreLabel)
        
            // Setting up restart button
        restartLabel.makeTextPulsating("Restart", theFontSize: 70)
        restartLabel.fontColor = SKColor.whiteColor()
        restartLabel.position = CGPoint(x:self.size.width / 2, y: self.size.height * 0.3)
        restartLabel.zPosition = 1
        self.addChild(restartLabel)
        
            // Setting up quit button
        quiteLabel.text = "Quit"
        quiteLabel.fontSize = 70
        quiteLabel.fontColor = SKColor.whiteColor()
        quiteLabel.position = CGPoint(x:self.size.width / 2, y: self.size.height * 0.1)
        quiteLabel.zPosition = 1
        self.addChild(quiteLabel)
        
    }
    
    // Return to scene view function
//    func returnToScene(setScene : SKScene){
//        let setScene:SKScene = SKScene(size:self.size)
//        let sceneToMoveTo = setScene
//        let transitionType = SKTransition.doorsCloseHorizontalWithDuration(0.5)
//        self.view!.presentScene(sceneToMoveTo, transition: transitionType)
//        
//    }
    
        // Making touch on restart label
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches{
                // Where is location in touch of self-cordinate of touch
            let touchPoint = touch.locationInNode(self)
                // Checking if restart label position cotains touch
            if restartLabel.containsPoint(touchPoint) {
                // sending to the game scene
                let sceneToMoveTo = GameScene(size:self.size)
                sceneToMoveTo.scaleMode = scaleMode
                let transitionType = SKTransition.flipVerticalWithDuration(0.5)
                // Presening the view
                self.view!.presentScene(sceneToMoveTo, transition: transitionType)
            }
            
            // If quite button touched
            if quiteLabel.containsPoint(touchPoint) {
                // sending to the game scene
                let sceneToMoveTo = StartGameScene(size:self.size)
                sceneToMoveTo.scaleMode = scaleMode
                let transitionType = SKTransition.doorsCloseVerticalWithDuration(0.5)
                // Presening the view
                self.view!.presentScene(sceneToMoveTo, transition: transitionType)
            }
        }
    }
    
}
