//
//  PulsatingText.swift
//  LejlaBelma
//
//  Created by Ermin on 2016-05-22.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

import Foundation
import SpriteKit


/// Class to make text puls size take default pramaeter from super class
class PulsatingText: SKLabelNode {
    
    func makeTextPulsating(theText:String, theFontSize:CGFloat){
        // Setting text
        self.text = theText
        // Setting fontsize
        self.fontSize = theFontSize
        // Make text pulsating with sequence
        let scaleSequence = SKAction.sequence([SKAction.scaleTo(2, duration: 1),SKAction.scaleTo(1.0, duration: 1)])
        // Making scale forever
        let scaleForever = SKAction.repeatActionForever(scaleSequence)
        self.runAction(scaleForever)
    }
}
