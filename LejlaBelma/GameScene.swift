//
//  GameScene.swift
//  LejlaBelma
//
//  Created by Ermin on 2016-05-20.
//  Copyright (c) 2016 Ermin Mahmutovic. All rights reserved.
//

import SpriteKit
import MediaPlayer

    // Setting up categories to react with bullet, player,enemy and non
struct PhysicCategories {
    static let   None: UInt32 = 0
    static let Player: UInt32 = 0b1 // bynary one 1
    static let Bullet: UInt32 = 0b10  // bynary two 2
    static let  Enemy: UInt32 = 0b100 // bynary 4
}

    // Enum for the gamstate
enum gameState{
    case preGame // when the game is before its starts
    case inGame  // When the game is on the game
    case afterGame // After the game
}
        // Game score
    var gameScore = 0
class GameScene: SKScene, SKPhysicsContactDelegate{
    
    //Settting curent game status
    var currentGameState = gameState.inGame

        // Level numer
    var levelNumber = 0
        // Lives number
    var playerLives = 3
    
    let livesLabel = SKLabelNode(fontNamed:"ChalkDuster")
    let scoreLabel = SKLabelNode(fontNamed:"ChalkDuster")

        // setting up the player ship
    let player = SKSpriteNode(imageNamed: "playerShip")
        // Adding bullet sound
    let bulletSound = SKAction.playSoundFileNamed("laser.mp3", waitForCompletion: false) // Dont wait for compilation just play then move bullet
    let explosionSound = SKAction.playSoundFileNamed("explosion1.mp3", waitForCompletion: false)
    
        // Random function to get random position of enemies in the screen
    func random() -> CGFloat{
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    func random(min min:CGFloat, max:CGFloat) -> CGFloat {
        return random() * ( max - min) + min
    }
        // Getting game area maring with rectangle value
    var gameArea:CGRect
        // Getting rect value from the initializer
    override init(size: CGSize) {
        let aspectRatio : CGFloat = 16.0 / 9.0
        let playableWidth = size.height / aspectRatio //how withd our game area gone be
        let margin = (size.width - playableWidth) / 2// diferense of how with the scene is and how with the play area is
        gameArea = CGRect(x: margin,y: 0,width: playableWidth,height: size.height)
            // Game area x= margin, y= 0 from left bottom corner, heigth and screen hight
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        // Reseting gamescore became 0 everytime we came to the game
        gameScore = 0
        
            // setting up phisic contact delegete
        self.physicsWorld.contactDelegate = self
        
        player.setScale(1) // Original size of spaceship
        player.position = CGPoint(x: self.size.width / 2,y:self.size.height * 0.2) // 20%  from bottom position
        player.zPosition = 2 // zposition 1 gone be bullet
            // Adding body to player size
        player.physicsBody = SKPhysicsBody(rectangleOfSize: player.size)
            // remove gravity of player body
        player.physicsBody!.affectedByGravity = false
            // Take the physicbody of the player and put it to the player category
        player.physicsBody!.categoryBitMask = PhysicCategories.Player
            // Adding collision and contact
        player.physicsBody!.collisionBitMask = PhysicCategories.None
        player.physicsBody!.contactTestBitMask = PhysicCategories.Enemy
        
        
        self.addChild(player) // Adding player ship to view
        
            // Adding label for score
        scoreLabel.text = "Score: 0"
        scoreLabel.fontSize = 70 // Size of font
        scoreLabel.fontColor = SKColor.whiteColor() // Color font
            // Aligne text to the left of the screen
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
            // 15% to left width of the screen and 90% of the heigth of the screen
        scoreLabel.position = CGPoint(x:self.size.width * 0.15,y:self.size.height * 0.9)
        scoreLabel.zPosition = 100 // Addind label on the top of all objects
            // Adding child to game
        self.addChild(scoreLabel)
        
            // Adding lives label
        livesLabel.text = "Lives: 3"
        livesLabel.fontSize = 70
        livesLabel.fontColor = SKColor.whiteColor()
        livesLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        livesLabel.position = CGPoint(x:self.size.width * 0.85,y:self.size.height * 0.9)
        livesLabel.zPosition = 100
        self.addChild(livesLabel)
        
        // Adding startfields
        let starField = SKEmitterNode(fileNamed:"StarField")
        starField?.position = CGPoint(x:self.size.width / 2, y:self.size.height / 2)
        starField?.zPosition = -100
        self.addChild(starField!)
        
            // Starting new level
        startNewLevel()

    }
    
        // Func to lose a life
    func loseALife(){
        playerLives -= 1
        livesLabel.text = "Lives: \(playerLives)"
        
            // Scaling label if a life is gone
        let scaleUp = SKAction.scaleTo(1.5, duration: 0.2)
        let scaleDown = SKAction.scaleTo(1, duration: 0.2)
        // Setting these as a sequence
        let scaleSequence = SKAction.sequence([scaleUp,scaleDown])
        livesLabel.runAction(scaleSequence)
        
            //running gameover if player lives is 0
        if playerLives == 0 {
            runGameOver()
        }
    }
    
        // adding score to game
    func addScore(){
        gameScore += 1
        scoreLabel.text = "Score: \(gameScore)"
        
            // Starting level after gamescore reach score
        if gameScore == 15 || gameScore == 30 || gameScore == 50 || gameScore == 80 || gameScore == 100 {
            startNewLevel()
        }
    }
    
        // Method to running game over
        // Handle when we wont to end the game
    func runGameOver(){
        
            // That knows when the current game is finished
        currentGameState = gameState.afterGame
        
            // Stopp all action when gameover
        self.removeAllActions()
        
            // Adding bullet refferens
            // Genrete all nodes with this name ("Bullet") and stop them
        self.enumerateChildNodesWithName("Bullet"){
            bullet, stop in // lopping all bullets and stop them to run
            bullet.removeAllActions()
        }
        
            // Adding enemy refference
            // generate all enemyes and with this name and stop the,
        self.enumerateChildNodesWithName("Enemy"){
            enemy, stop in // looping all enemies add stop them
            enemy.removeAllActions()
        }
            // Runnig the block when the game over 
        let changeSceneAction = SKAction.runBlock(changeScene)
        let waitTochangeScene = SKAction.waitForDuration(1.0)
        let changeSequence = SKAction.sequence([waitTochangeScene,changeSceneAction])
        self.runAction(changeSequence)
    }
    
    
    // Change to game over scene when lose
    func changeScene(){
        
        // Letting move to scene
        let sceneToMoveTo = GameOverScene(size:self.size)
        // Scaling mode to game over scene same as current scene
        sceneToMoveTo.scaleMode = self.scaleMode
        // Making transition to next scene
        let transitionType = SKTransition.flipHorizontalWithDuration(0.5)
        // Making all this abow happen
        // Take the view and present new scene
        self.view!.presentScene(sceneToMoveTo, transition: transitionType)
    }
    
        // Checking contact of the objects-This gone run when two physic object contact each other
    func didBeginContact(contact: SKPhysicsContact) {
        // Organize bodyA and bodyB to know witch object it is
        var body1 = SKPhysicsBody()
        var body2 = SKPhysicsBody()
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            body1 = contact.bodyA
            body2 = contact.bodyB
        }else{
            body1 = contact.bodyB
            body2 = contact.bodyA
        }
        
            // If the player and enemy touch eash other 
        if body1.categoryBitMask == PhysicCategories.Player && body2.categoryBitMask == PhysicCategories.Enemy {
            
                // running twice spawn explotion 1: player 2: enemy
                // SpawExpol. in the position of the player
            if body1.node != nil {
                spawnExpolosion(body1.node!.position)
            }
                // Spawexplot. in the position of the enemy
            if body2.node != nil {
                spawnExpolosion(body2.node!.position)
            }
                // Find the body one  and body 2 node and delete it
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            
            // Running game over
            runGameOver()
        }
            // If the bullet and the enemy hit eash other
            // And if the enemy ois on the screen
        if body1.categoryBitMask == PhysicCategories.Bullet && body2.categoryBitMask == PhysicCategories.Enemy && body2.node?.position.y < self.size.height{
            
                // When bullet hit the enemy add score
            addScore()
                // Spawn expploation only body 2= enemy
            if body2.node != nil {
                spawnExpolosion(body2.node!.position)
            }
                // Find body one and two and delete it
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
        }
    }
        // This function gone show explosion when it contanct with bullet/enemy
    func spawnExpolosion(spawPosition:CGPoint){
        let explosion = SKSpriteNode(imageNamed: "explosion")
        // Setting position as argumnet position
        explosion.position = spawPosition
        explosion.zPosition = 3 // Top of the ship
        explosion.setScale(0) // Scale the expolotion to invisible
        self.addChild(explosion)
        
        // Handle expoltion to shows bigger on hit
        let scaleIn = SKAction.scaleTo(1, duration: 0.1)
        let scaleFadeOut = SKAction.fadeOutWithDuration(0.1)
        let deleteExplosion = SKAction.removeFromParent()
        let explosionSequence = SKAction.sequence([explosionSound,scaleIn,scaleFadeOut,deleteExplosion])
        explosion.runAction(explosionSequence)
    }
    
        // function to fire bullet
    func fireBullet(){
            // getting buller from object
        let bullet = SKSpriteNode(imageNamed: "bullet")
        
            // Making a reference of the bullet
        bullet.name = "Bullet"
        bullet.setScale(1) // Setting scale to original image size
        bullet.position = player.position // Same as ship position
        bullet.zPosition = 1 // Position under the ship i z cordinate
            // Adding physicbody to the bullet and removing gravity
        bullet.physicsBody = SKPhysicsBody(rectangleOfSize: bullet.size)
        bullet.physicsBody!.affectedByGravity = false
            // Take the physicsbody of the bullet and putt it to the category of bullet
        bullet.physicsBody!.categoryBitMask = PhysicCategories.Bullet
            // Adding collision and contact
        bullet.physicsBody!.collisionBitMask = PhysicCategories.None
        bullet.physicsBody!.contactTestBitMask = PhysicCategories.Enemy
        
        self.addChild(bullet) // adiing bullet to scene
        
            // moving action bullet to Y top of the screen add delete it after
        let movingBullet = SKAction.moveToY(self.size.height + bullet.size.height, duration: 1) // Moving duration 1 second
        let deleteBullet = SKAction.removeFromParent() // Removing bullet from game to prevent memory leak
        
            // This is list of action that runs in order in one after the other
        let bulletSequence = SKAction.sequence([bulletSound,movingBullet,deleteBullet])
        bullet.runAction(bulletSequence) // Running this sequence
        
    }
    
        // Making an function to show enemy on screen
    func spawnEnemy(){
        let randomXStart = random(min: CGRectGetMinX(gameArea), max: CGRectGetMaxX(gameArea))
        let randomXEnd = random(min: CGRectGetMinX(gameArea), max: CGRectGetMaxX(gameArea))
        
            // -20% from top
        let startPoint = CGPoint(x:randomXStart,y:self.size.height * 1.2)
            // +20% from bottom
        let endPoint = CGPoint(x: randomXEnd, y: -self.size.height * 0.2)
        
            // Getting the enemy
        let enemy = SKSpriteNode(imageNamed: "enemyShip")
        enemy.name = "Enemy"
        enemy.setScale(1) // size of enemy ship
        enemy.position = startPoint
        enemy.zPosition = 2
            // Adding phisicbody to the enemmy nd removing gravity
        enemy.physicsBody = SKPhysicsBody(rectangleOfSize: enemy.size)
        enemy.physicsBody!.affectedByGravity = false
            // Take the physicbody of the enemy and put it to the category of the enemy
        enemy.physicsBody!.categoryBitMask = PhysicCategories.Enemy
            // Adding collision to nothing and cantact on bullet and player
        enemy.physicsBody!.collisionBitMask = PhysicCategories.None
        enemy.physicsBody!.contactTestBitMask = PhysicCategories.Bullet | PhysicCategories.Player
        
        self.addChild(enemy)
        
            // Moving enemy to bottom of screen
        let moveEnemy = SKAction.moveTo(endPoint, duration: 3.0)
        let deleteEnemy = SKAction.removeFromParent()
        let loseALifeAction = SKAction.runBlock(loseALife) // CAlling block to lose a life, and adding that to sequence action
        let enemySequece = SKAction.sequence([moveEnemy,deleteEnemy, loseALifeAction])
        
            // runs enemy if state is in game
        if currentGameState == gameState.inGame {
            enemy.runAction(enemySequece) // Running the enemy
        }
        
            // deference beetween x and y
        let dx = endPoint.x - startPoint.x
        let dy = endPoint.y - startPoint.y
        
        let amountToRotate = atan2(dy, dx)
        enemy.zRotation = amountToRotate
    }
    
        // starting new level
    func startNewLevel(){
        //Adding one level
        levelNumber += 1
        
        // adding condision if key are not nil
        if self.actionForKey("spawningEnemy") != nil{
            self.removeActionForKey("spawningEnemy")
        }
        
        // Handle speed of every level
        var levelDuration = NSTimeInterval()
        
        switch levelNumber {
        case 1:
            levelDuration = 1.5
        case 2:
            levelDuration = 1.2
        case 3:
            levelDuration = 1.0
        case 4:
            levelDuration = 0.8
        case 5:
            levelDuration = 0.6
        case 6:
            levelDuration = 0.5
        default:
            levelDuration = 0.5
            print("Can't find more levels")
        }
        
            // lopping to run the enemyes
        let spawn = SKAction.runBlock(spawnEnemy)
            // Making pause of enemyes
        let waitToSpawn = SKAction.waitForDuration(levelDuration) // Every level duration
            // Ordering the enemy
        let spawSequence = SKAction.sequence([waitToSpawn,spawn])
            // makin infinty calling spawn
        let spawnForever = SKAction.repeatActionForever(spawSequence)
            // running the action on screen
        // These actions runs when calling key "spawwningEnemy"
        self.runAction(spawnForever, withKey: "spawningEnemy")
    }
    
        // Fire the bullet on tap on screen
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // Only fire the bullet if gamestate is ingame
        if currentGameState == gameState.inGame {
            fireBullet()
        }
        
    }
        // Action when user move finger somewhere
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch:AnyObject in touches {
                // Point of tuche where we touch the screen right now
                // location on tuoch in the scene - self
            let pointOfTouch = touch.locationInNode(self)
                // this is where touch was before it toched
            let previosPointOfTouch = touch.previousLocationInNode(self)
                // This is distance between touches
            let amountDragged = pointOfTouch.x - previosPointOfTouch.x
            
                // Moving the player ship position to distance value
                // And only do the folowing if gamestate is in game
            if currentGameState == gameState.inGame {
                player.position.x += amountDragged
            }
            
                // Checking if object goes out of screen to rigth
            if player.position.x > CGRectGetMaxX(gameArea) - player.size.width / 2{
                player.position.x = CGRectGetMaxX(gameArea) - player.size.width / 2
            }
                // Same check but to left side of screen
            if player.position.x < CGRectGetMinX(gameArea) + player.size.width / 2{
                player.position.x = CGRectGetMinX(gameArea) + player.size.width / 2
            }
            
        }
    }
}
